from django.test import SimpleTestCase
from django.urls import reverse, resolve
from backend.views import home, login

class TestUrls(SimpleTestCase):
    def test_home_path_name_points_to_home_route(self):
        '''Check if the path name 'home' resolve to the correct URL'''
        path = reverse('home')
        self.assertEquals(resolve(path).route, '')

    def test_home_url_points_to_home_view(self):
        '''Check if the path name 'home' resolve to the correct view'''
        path = reverse('home')
        self.assertEquals(resolve(path).func, home)

    def test_login_path_name_points_to_login_route(self):
        '''Check if the path name 'login' resolve to the correct URL'''
        path = reverse('login')
        self.assertEquals(resolve(path).route, 'login/')

    def test_login_url_points_to_login_view(self):
        '''Check if the path name 'login' resolve to the correct view'''
        path = reverse('login')
        self.assertEquals(resolve(path).func, login)