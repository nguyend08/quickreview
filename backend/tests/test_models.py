from django.test import TestCase
from django.db import models
from backend.models import User

class TestModels(TestCase):
    def setUp(self):
        '''This function will be executed before running all the tests. Best used when declaring common variables or
        when certain things need to be set up before running tests.
        '''
        self.permissions = {'auth.add_logentry','auth.change_logentry','auth.delete_logentry','auth.view_logentry',
                            'auth.add_permission', 'auth.change_permission', 'auth.delete_permission', 'auth.view_permission',
                            'auth.add_group', 'auth.change_group', 'auth.delete_group', 'auth.view_group',
                            'auth.add_contenttype', 'auth.change_contenttype', 'auth.delete_contenttype', 'auth.view_contenttype',
                            'auth.add_session', 'auth.change_session', 'auth.delete_session', 'auth.view_session',
                            'auth.add_user','auth.change_user','auth.delete_user','auth.view_user',
                            'backend.add_logentry', 'backend.change_logentry', 'backend.delete_logentry', 'backend.view_logentry',
                            'backend.add_permission', 'backend.change_permission', 'backend.delete_permission',
                            'backend.view_permission',
                            'backend.add_group', 'backend.change_group', 'backend.delete_group', 'backend.view_group',
                            'backend.add_contenttype', 'backend.change_contenttype', 'backend.delete_contenttype',
                            'backend.view_contenttype',
                            'backend.add_session', 'backend.change_session', 'backend.delete_session', 'backend.view_session',
                            'backend.add_user','backend.change_user','backend.delete_user','backend.view_user'}

    def test_user_model_correct_type(self):
        '''Make sure the fields on the User model have correct data types'''
        self.assertTrue(type(User._meta.get_field("email")) == models.EmailField)
        self.assertTrue(type(User._meta.get_field("password")) == models.CharField)
        self.assertTrue(isinstance(User._meta.get_field("active"), models.BooleanField))
        self.assertTrue(isinstance(User._meta.get_field("staff"), models.BooleanField))
        self.assertTrue(isinstance(User._meta.get_field("admin"), models.BooleanField))
        self.assertTrue(isinstance(User._meta.get_field("confirm"), models.BooleanField))
        self.assertTrue(isinstance(User._meta.get_field("timestamp"), models.DateTimeField))

    def test_avg_user_perm(self):
        '''Make sure the average users do not have admin or staff permission'''
        avg_user = User.objects.create_user('user123@email.com','user123','user123')
        self.assertFalse(avg_user.is_superuser)
        self.assertFalse(avg_user.is_staff)
        for perm in self.permissions:
            self.assertFalse(avg_user.has_perm(perm))

    def test_superuser_perm(self):
        '''Make sure the superusers have all the permissions'''
        superuser = User.objects.create_superuser('user123@email.com','user123','user123')
        self.assertTrue(superuser.is_superuser)
        self.assertTrue(superuser.is_staff)
        for perm in self.permissions:
            self.assertTrue(superuser.has_perm(perm))

    def test_user_password_hashed(self):
        '''Make sure the users password is hashed'''
        user = User.objects.create_user('user123@email.com', 'user123', 'user123')
        # ensure password is not stored in plaintext
        self.assertNotEqual(user.password, 'user123')
        # check if hashed with argon2 algorithm
        self.assertEqual(user.password.split('$')[0], 'argon2')
        # check if the password is hashed correctly
        assert user.check_password('user123')