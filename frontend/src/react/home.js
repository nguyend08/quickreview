import React from "react";

import ReactDOM from "react-dom";

const Home = () => {
    return <i>This component is created in React</i>;
};

ReactDOM.render(<Home />, document.getElementById("app"));
